# zSearch
Building a cross-platform search application, based on the in progress cross platform library zLib.

Objectives:

* Exercise / practice TDD.
* Learn & understand more of Linux API.
* Do not use Qt.

The objective of this project is not to reach a fully-functional application for possible end users to enjoy.

By this, the functionality of the old SearchApp (https://github.com/Feoggou/searchapp) will be rewritten, in a more professional manner.

A previous attempt of rewriting that project had been https://github.com/Feoggou/old.zSearch, which was finally abandoned because its development strayed too much on the "design upfront" path.

# zshell

## Purpose
### Product Vision (Why the project deserves to exist)
Create a cross-platform interpreter with a unified command interface for managing the OS

Q: What should the project accomplish? (problem / opportunity that the project will address, expressed as an end result)

A: A simpler, more intuitive shell interpreter, that can replace bash and its utilities. Cross-platform (mainly unix, I'm not sure about windows since many sys admins on windows may currently use either UI or specific WMI stuff).

Q: Why is the project valuable?

A: To customers: will be able to use zshell (instead of python / bash) for sys admin tasks, which will make their work (create, edit, maintain, test, debug) easier.
   To me: ?

Q: (Success Criteria) How will you know that the project succeeded and when will you decide?

A: When I have a MVP and someone else chooses to use it instead of bash.

### Project Mission
Users: Windows & Linux system administrators

Benefits:

* A single language for handling both Windows and Linux (possibly, Solaris also) means less headache when in need to automate OS tasks.
* A unified command interface means that any kind of command and any utility can be wrapped around the same command interface, which provides familiar syntax for any utility. (e.g. the command "delete" can be used on files, on directories, on processes, etc. with aliases "delete" = "remove")
* intuitive: not many key symbols, and the syntax should be easy to come up with.
* easy help & contextual help: don't know what a line of text does? Ask the interpreter!
* different style from script to interactive: script should require full names (i.e. no abbreviations) and aim for readability and maintainability, while the interactive one should focus on providing context help, suggestions, and allowing abbreviations for quick writing - though with abbreviations, in the modern terminals, the tab key allows for quickly writing long names, so that might not be necessary.
* allow plugins, extendable (with new utilities), customizable: easy to add commands, providers, etc, easy to set defaults, configurations and aliases.


### Mission Tests
Must provide support for the following:

* Filesystem
* Processes & threads
* File IO
* strings
* regular expressions & search