/*
*  Copyright 2015 Samuel Ghineț
*  
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*  
*      http://www.apache.org/licenses/LICENSE-2.0
*  
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

#include "zShell.h"
#include "Command.h"

#include <iostream>


Zen::ZShell::ZShell(CommandIterator& iter)
    : iterator(iter)
{}

int Zen::ZShell::execute(const std::string& scriptContent, std::string& stdOutContent)
{
    int error = 0;

    iterator.setScriptContent(scriptContent);

    while (std::unique_ptr<Command> cmd = iterator.readNext())
    {
        error = cmd->execute(stdOutContent);

        if (error) {
            return error;
        }
    }

    return 0;
}

void Zen::ZShell::outputToStdout(const std::string &message) const
{
    std::cout << message;
}
