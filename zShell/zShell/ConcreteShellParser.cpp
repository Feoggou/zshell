#include "ConcreteShellParser.h"

#include "ExitCommand.h"

#include "zLib/ConcreteTextFile.h"

#include "fs/Create.h"
#include "con/Echo.h"

#include "Config.h"

#include <iostream>
#include <cassert>


Zen::ConcreteShellParser::ConcreteShellParser(std::shared_ptr<TextFile> textFile_)
    : textFile { textFile_ }
{
    if (!textFile)
    {
        textFile.reset(new Zen::ConcreteTextFile);
    }
}

std::string Zen::ConcreteShellParser::readCommandArgs(const std::string& cmdName, std::string content) const
{
    std::string arguments = content.erase(0, cmdName.length() + 1);

    size_t newLine = arguments.find('\n');
    if (newLine != arguments.npos)
    {
        arguments.resize(newLine);
        content.erase(0, newLine + 1);
    }

    return arguments;
}

std::string Zen::ConcreteShellParser::readCommandName(const std::string &text) const
{
    size_t space = text.find(' ');
    size_t newline = text.find('\n');

    size_t idx = std::min(space, newline);

    return text.substr(0, idx);
}

// TODO: MUST refactor / rewrite this
// TODO: It shouldn't belong in this class
std::string Zen::ConcreteShellParser::getCommandFile(const std::string& commandName) const
{
    //TODO: what if builtin command such as 'echo' (atm)?

    //TODO: make path to "ucif" configurable. Either read it from standard place or let the path be set at command line.
    std::string filePath = Zen::ZSHELL_UCIF_DIR + commandName + ".cif";

    assert(textFile);

    //TODO: what if the file path / UCIF does not exist?
    return textFile->readAll(filePath);
}

// TODO: MUST rewrite. This is a terribly simplified functionality.
// TODO: I don't think it should belong in this class
Zen::Command* Zen::ConcreteShellParser::buildCommandInterface(const std::string& content) const
{
    bool cmdIsCreate = true;
    size_t index = content.find("fs@create", 0);
    if (index == -1)
    {
        index = content.find("echo", 0);
        cmdIsCreate = false;
    }

    bool cmdIsEcho = true;
    if (index == -1)
    {
        index = content.find("exit", 0);
        cmdIsEcho = false;
    }

    if (index == -1)
        return nullptr;

    //TODO: leak?
    //TODO: I think arguments are not being parsed correctly. Have no test for this.
    if (cmdIsCreate)
        return new Zen::CreateCommand;
    else if (cmdIsEcho)
        return new Zen::EchoCommand;
    else
        return new Zen::ExitCommand;
}
