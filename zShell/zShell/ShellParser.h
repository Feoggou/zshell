#pragma once

#include <string>

namespace Zen {

class Command;

/**
 * TODO: We haven't considered the separation between Interface and Implementation yet.
 *       Basically, we can't construct a Command object before we find an implementation for this interface.
 *
 * NOTE: A Command Interface represents the signature of a command (name, args, types of args, number of args)
 */
class ShellParser
{
public:
    virtual ~ShellParser() = default;

    /**
     * @brief readCommandName
     *      Find and extract the next command name from line / text
     * @param line
     *      The text to extract the command from
     * @return
     *      The name of the command
     */
    virtual std::string readCommandName(const std::string& line) const = 0;

    /**
     * @brief readCommandArgs
     *      Given a text "content", strips away the "cmdName", and returns the text representing the arguments.
     * @param cmdName
     *      The name of the command.
     *      TODO: With our without namespace?
     * @param content
     *      the text content, from the script file.
     * @return
     *      the text string representing the arguments of the command.
     */
    virtual std::string readCommandArgs(const std::string& cmdName, std::string content) const = 0;

    /**
     * @brief getCommandFile
     *      Given the name of a command, retrieve the content of the corresponding file
     *      TODO: What should it do if it cannot find it?
     * @param commandName
     *      The name of the command to find UCIF for.
     *      TODO: What if it's a builtin command?
     * @return
     *      The content of the UCIF file (string)
     */
    virtual std::string getCommandFile(const std::string& commandName) const = 0;

    /**
     * @brief buildCommandInterface
     *      Given the content of a UCIF file, return a Command object that matches it.
     * @param content
     *      The content of the UCIF file
     * @return
     *      A constructed Command object.
     *      NOTE: caller is responsible with deleting allocated memory.
     */
    virtual Zen::Command* buildCommandInterface(const std::string& content) const = 0;
};

}
