#include "ConcreteCommandIterator.h"

#include "ConcreteShellParser.h"

#include "Command.h"


Zen::ConcreteCommandIterator::ConcreteCommandIterator(std::shared_ptr<ShellParser> parser_)
    : shellParser { parser_ }
{
    if (!shellParser)
    {
        shellParser.reset(new Zen::ConcreteShellParser);
    }
}

std::unique_ptr<Zen::Command> Zen::ConcreteCommandIterator::readNext()
{
    if (scriptContent.empty())
        return nullptr;

    std::string cmdName = shellParser->readCommandName(scriptContent);

    if (cmdName.empty())
    {
        //ATM the only time this happens is: "cmd <args>\n\n\n"
        return nullptr;
    }

    std::string content = shellParser->getCommandFile(cmdName);
    std::unique_ptr<Zen::Command> p { shellParser->buildCommandInterface(content) };

    std::string arguments = shellParser->readCommandArgs(cmdName, scriptContent);
    p->setArguments(arguments);

    size_t toErase = cmdName.length() + 1; // 1 = for last '\n'

    if (!arguments.empty())
        toErase += arguments.length() + 1; //1 = for intermediary ' '

    scriptContent.erase(0, toErase);

    return p;
}
