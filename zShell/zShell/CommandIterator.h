#pragma once

#include <string>
#include <memory>


namespace Zen {
class Command;

/**
 * @brief The CommandParser class
 *
 * Responsibility:
 *      Given a script file once, it provides the Commands, one after another, from the script file.
 *
 * Collaborators:
 *      CommandInterface: In order to extract Command object
 */
class CommandIterator
{
public:
    virtual ~CommandIterator() = default;

    virtual void setScriptContent(const std::string&) = 0;
    virtual std::unique_ptr<Command> readNext() = 0;
};

}
