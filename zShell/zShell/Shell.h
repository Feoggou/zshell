#pragma once

#include <string>
#include <memory>

namespace Zen {

class TextFile;


/**
 * @brief The Shell class
 * Responsibity: performs the execution of a script given a script file.
 *               In the future, possibly: also of a line of input from terminal (interactive).
 */
class Shell
{
public:
    explicit Shell(std::shared_ptr<TextFile> textFile = nullptr);
    virtual ~Shell() = default;

    int runScript(const std::string& scriptFile);

protected:
    virtual int execute(const std::string& scriptContent, std::string& stdOutContent) = 0;
    virtual void outputToStdout(const std::string& message) const = 0;

protected:
    std::shared_ptr<TextFile> textFile;
};

}
