#pragma once

#include "CommandIterator.h"

#include <string>
#include <memory>


namespace Zen {

class ShellParser;


//TODO: Consider changing CommandParserImpl<MockCommandInterface> with CommandParserImpl<TCommandInterface>
class ConcreteCommandIterator final : public CommandIterator
{
public:
    explicit ConcreteCommandIterator(std::shared_ptr<ShellParser> parser = nullptr);

    void setScriptContent(const std::string& content) override { scriptContent = content; }
    std::unique_ptr<Command> readNext() override;

private:
    std::string scriptContent;
    std::shared_ptr<Zen::ShellParser> shellParser;
};
}
