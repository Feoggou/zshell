#pragma once

#include "Command.h"

#include <string>

namespace Zen {
// NOTE: Temporary class (will be read from a script file)
class ExitCommand: public Command
{
public:
    int execute(std::string&) override
    {
        std::string args = arguments();

        if (args.empty())
            return 0;

        return std::stoi(args);
    }

    std::string name() const override { return "exit"; }
};

}
