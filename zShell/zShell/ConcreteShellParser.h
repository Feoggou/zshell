#pragma once

#include "ShellParser.h"

#include <memory>

namespace Zen {

class TextFile;

class ConcreteShellParser: public ShellParser
{
public:
    explicit ConcreteShellParser(std::shared_ptr<TextFile> textFile = nullptr);

    std::string readCommandName(const std::string& text) const override;
    std::string readCommandArgs(const std::string& cmdName, std::string content) const override;
    std::string getCommandFile(const std::string& commandName) const override;
    Zen::Command* buildCommandInterface(const std::string& content) const override;

private:
    std::shared_ptr<TextFile> textFile;
};

}
