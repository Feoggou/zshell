/*
*  Copyright 2015 Samuel Ghineț
*  
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*  
*      http://www.apache.org/licenses/LICENSE-2.0
*  
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

#include "Shell.h"

#include "zLib/ConcreteTextFile.h"


Zen::Shell::Shell(std::shared_ptr<TextFile> textFile_)
    : textFile { textFile_ }
{
    if (!textFile_)
        textFile.reset(new Zen::ConcreteTextFile);
}

int Zen::Shell::runScript(const std::string& scriptFile)
{
    std::string content = textFile->readAll(scriptFile);
    std::string stdOutContent;

    int exitCode = execute(content, stdOutContent);
    outputToStdout(stdOutContent);

    return exitCode;
}
