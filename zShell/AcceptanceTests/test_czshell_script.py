import unittest
import subprocess
import os


class TestCZShell(unittest.TestCase):
    EXE = "./zshell"

    @classmethod
    def setUpClass(cls):
        os.chdir("./bin")
        print ("changed dir to bin")

    def cZShell_withScript_test(self, script: str, expected: str):
        completed_proc = subprocess.run([self.EXE, "-s", script], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        actual = completed_proc.stdout.decode("utf-8")
        errors = completed_proc.stderr.decode("utf-8")

        self.assertIs("", errors)
        self.assertEqual(expected, actual)
        self.assertEqual(0, completed_proc.returncode);

    def test_echo(self):
        self.cZShell_withScript_test("echo.zsh", "mymessage\n")

    def test_echoOtherMessage(self):
        self.cZShell_withScript_test("echo_other.zsh", "other-message\n")

    def test_echo_withoutEmptyLine(self):
        self.cZShell_withScript_test("echo_no_empty_line.zsh", "just-one-line\n")

    def test_whenScriptDoesNotExist_OutputToStderr_and_returnNonZero(self):
        completed_proc = subprocess.run([self.EXE, "-s", "<nonexisting>"], stderr=subprocess.PIPE)
        errors = completed_proc.stderr.decode("utf-8")

        self.assertEqual("File '<nonexisting>' does not exist\n", errors)
        self.assertNotEqual(0, completed_proc.returncode)

    def test_exit34_returnsExitCode_34(self):
        completed_proc = subprocess.run([self.EXE, "-s", "exit34.zsh"], stderr=subprocess.PIPE)

        self.assertEqual(34, completed_proc.returncode)

    def test_exit_returnsExitCode_0(self):
        completed_proc = subprocess.run([self.EXE, "-s", "exit.zsh"], stderr=subprocess.PIPE)

        self.assertEqual(0, completed_proc.returncode)

    def test_twoCommands_echoAndExit(self):
        completed_proc = subprocess.run([self.EXE, "-s", "two-commands.zsh"], stdout=subprocess.PIPE)
        actual = completed_proc.stdout.decode("utf-8")

        self.assertEqual(251, completed_proc.returncode)
        self.assertEqual("exiting...\n", actual)


if __name__ == '__main__':
    unittest.main()
