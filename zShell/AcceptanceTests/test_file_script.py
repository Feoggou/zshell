import unittest
import subprocess
import os


class TestFile(unittest.TestCase):
    EXE = "./zshell"

    @classmethod
    def setUpClass(cls):
        os.chdir("./bin")
        print ("changed dir to bin")

    def tearDown(self):
        if os.path.exists("test-createfile.txt"):
                os.remove("test-createfile.txt")

    def test_createFile(self):
        completed_proc = subprocess.run([self.EXE, "-s", "create-file.zsh"])

        self.assertEqual(0, completed_proc.returncode)
        self.assertTrue(os.path.isfile("test-createfile.txt"))


if __name__ == '__main__':
    unittest.main()

