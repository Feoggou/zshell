/*
*  Copyright 2015 Samuel Ghineț
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

#include "zShell/zShell.h"
#include "zShell/ConcreteShellParser.h"

#include "zLib/TextFile.h"

#include "fs/Create.h"

#include "Config.h"

#include "gmock/gmock.h"

using ::testing::Return;
using ::testing::SetArgReferee;
using ::testing::DoAll;
using ::testing::_;


class MockTextFile: public Zen::TextFile
{
public:
    MOCK_METHOD1(readAll, std::string(const std::string&));
};


TEST(Command, readCommandName_fromCifContent)
{
    Zen::ConcreteShellParser parser;
    std::string line = "fs@create file";

    auto commandName = parser.readCommandName(line);

    ASSERT_EQ("fs@create", commandName);
}

TEST(Command, getCommandFile_readsContentFromCif)
{
    auto textFile = std::make_shared<MockTextFile>();

    Zen::ConcreteShellParser parser { textFile };
    std::string fileContent = "content";
    std::string filePath = Zen::ZSHELL_UCIF_DIR + "fs@create.cif";

    EXPECT_CALL(*textFile, readAll(filePath))
            .WillOnce(Return("content"));

    std::string actual = parser.getCommandFile("fs@create");

    ASSERT_EQ(fileContent, actual);
}

TEST(Command, getCommandInterface_forCommandCreate_returnsCommandPtr)
{
    std::string cmdDescription = "SYNTAX\n"
        "# TODO: \"fs@\" here is temporary because we haven't implemented namespace lookup yet\n"
        "fs@create $what\n";
    Zen::ConcreteShellParser parser;

    std::unique_ptr<Zen::Command> command { parser.buildCommandInterface(cmdDescription) };

    ASSERT_NE(nullptr, command);
    ASSERT_EQ("fs@create", command->name());
}

TEST(Command, getCommandInterface_forCommandEcho_returnsCommandPtr)
{
    std::string cmdDescription = "SYNTAX\n"
        "echo $what\n";
    Zen::ConcreteShellParser parser;

    std::unique_ptr<Zen::Command> command { parser.buildCommandInterface(cmdDescription) };

    ASSERT_NE(nullptr, command);
    ASSERT_EQ("echo", command->name());
}


int main(int argc, char* argv[])
{
        ::testing::InitGoogleMock(&argc, argv);

        return RUN_ALL_TESTS();
}
