/*
*  Copyright 2015 Samuel Ghineț
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

#include "zShell/zShell.h"
#include "zShell/ConcreteCommandIterator.h"

#include "Command.h"
#include "zLib/TextFile.h"

#include "gmock/gmock.h"

using ::testing::Return;
using ::testing::ByMove;
using ::testing::SetArgReferee;
using ::testing::DoAll;
using ::testing::_;

//TODO: refactor

class MockShell: public Zen::Shell
{
public:
    explicit MockShell(std::shared_ptr<Zen::TextFile> textFile)
        : Shell(textFile)
    {}

    MOCK_METHOD2(execute, int(const std::string&, std::string&));
    MOCK_CONST_METHOD1(outputToStdout, void(const std::string&));
};

class MockTextFile: public Zen::TextFile
{
public:
    MOCK_METHOD1(readAll, std::string(const std::string& fileName));
};

class MockIterator: public Zen::CommandIterator
{
public:
    void setScriptContent(const std::string&) override {}

    MOCK_METHOD0(readNext, std::unique_ptr<Zen::Command>());
};


class MockCommand: public Zen::Command
{
public:
    std::string name() const override { return ""; }

    MOCK_METHOD1(execute, int(std::string&));
};

class ZShellTest: public ::testing::Test
{
protected:
    MockIterator iter;
    Zen::ZShell shell { iter };
    std::string stdOutContent;
};

/*******************************************/

TEST(ZShell, givenFileName_readsAndExecutesScript)
{
    auto textFile = std::make_shared<MockTextFile>();
    MockShell shell(textFile);

    EXPECT_CALL(*textFile, readAll("filename"))
            .WillOnce(Return(std::string("scriptcontent")));

    EXPECT_CALL(shell, execute("scriptcontent", _))
            .WillOnce(DoAll(SetArgReferee<1>("message"), Return(0)));

    EXPECT_CALL(shell, outputToStdout("message"));

    shell.runScript("filename");
}
TEST_F(ZShellTest, executeEcho_callsParser_executesCommandEcho)
{
    auto echoCmdPtr = std::make_unique<MockCommand>();
    echoCmdPtr->setArguments("message");

    MockCommand& echoCmd = *echoCmdPtr;

    EXPECT_CALL(iter, readNext())    //TODO: readNext()
            .WillOnce(Return(ByMove(std::move(echoCmdPtr))))
            .WillOnce(Return(ByMove(nullptr)));

    //TODO: should we also expect 'setScriptContent' to be called?
    EXPECT_CALL(echoCmd, execute(_));

    shell.execute("echo message", stdOutContent);
}

TEST_F(ZShellTest, when_haveTwoCommands_callReadNextCommand_thenExecuteEach)
{
    //MockCommand echoCmd, exitCmd;
    auto echoCmdPtr = std::make_unique<MockCommand>();
    auto exitCmdPtr = std::make_unique<MockCommand>();

    MockCommand& echoCmd = *echoCmdPtr;
    MockCommand& exitCmd = *exitCmdPtr;

    echoCmdPtr->setArguments("exiting...");
    exitCmdPtr->setArguments("-5");

    EXPECT_CALL(iter, readNext())
            .WillOnce(Return(ByMove(std::move(echoCmdPtr))))
            .WillOnce(Return(ByMove(std::move(exitCmdPtr))))
            .WillOnce(Return(ByMove(nullptr)));

    //TODO: should we also expect 'setScriptContent' to be called?
    EXPECT_CALL(echoCmd, execute(_));
    EXPECT_CALL(exitCmd, execute(_));

    shell.execute("<dummy>", stdOutContent);
}

TEST_F(ZShellTest, when_firstCommandFails_othersAreNotExecuted)
{
    auto failingCmdPtr = std::make_unique<MockCommand>();
    MockCommand& failingCmd = *failingCmdPtr;

    failingCmdPtr->setArguments("##??<error>");

    EXPECT_CALL(iter, readNext())
            .Times(1)
            .WillOnce(Return(ByMove(std::move(failingCmdPtr))));

    //TODO: should we also expect 'setScriptContent' to be called?
    EXPECT_CALL(failingCmd, execute(_))
            .WillOnce(Return(-4));

    int result = shell.execute("<dummy>", stdOutContent);

    ASSERT_EQ(-4, result);
}

int main(int argc, char* argv[])
{
        ::testing::InitGoogleMock(&argc, argv);

        return RUN_ALL_TESTS();
}
