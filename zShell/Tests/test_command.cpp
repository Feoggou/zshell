/*
*  Copyright 2015 Samuel Ghineț
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

#include "zShell/zShell.h"

#include "zShell/ExitCommand.h"

#include "con/Echo.h"

#include "gmock/gmock.h"

using ::testing::Return;
using ::testing::SetArgReferee;
using ::testing::DoAll;
using ::testing::_;


TEST(Command, echo_addsToStdoutString)
{
    Zen::EchoCommand cmd;
    cmd.setArguments("to-stdout");
    std::string stdOutString = "my-out ";

    int exitCode = cmd.execute(stdOutString);

    ASSERT_EQ("my-out to-stdout\n", stdOutString);
    ASSERT_EQ(0, exitCode);
}

TEST(Command, exit3_returnsExitCode)
{
    Zen::ExitCommand cmd;
    cmd.setArguments("3");
    std::string stdOutString;

    int exitCode = cmd.execute(stdOutString);

    ASSERT_EQ(3, exitCode);
}

TEST(Command, exit_returnsExitCode_0)
{
    Zen::ExitCommand cmd;
    std::string stdOutString;

    int exitCode = cmd.execute(stdOutString);

    ASSERT_EQ(0, exitCode);
}


int main(int argc, char* argv[])
{
        ::testing::InitGoogleMock(&argc, argv);

        return RUN_ALL_TESTS();
}
