/*
*  Copyright 2015 Samuel Ghineț
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

#include "zShell/zShell.h"

#include "zShell/ConcreteCommandIterator.h"
#include "zShell/ConcreteShellParser.h"

#include "fs/Create.h"

#include "gmock/gmock.h"

#include <memory>

using ::testing::Return;
using ::testing::SetArgReferee;
using ::testing::DoAll;
using ::testing::_;


#define ASSERT_CMD_EQ(cmd, cmdName, cmdArgs) { \
    ASSERT_NE(cmd, nullptr); ASSERT_EQ(cmdName, cmd->name()); ASSERT_EQ(cmdArgs, cmd->arguments()); \
}

Zen::ConcreteCommandIterator createIterator(const std::string& msg,
                                    std::shared_ptr<Zen::ShellParser> parser = std::make_shared<Zen::ConcreteShellParser>())
{
    Zen::ConcreteCommandIterator iter {parser};
    iter.setScriptContent(msg);

    return iter;
}

/***************************/

TEST(CommandParser, echoMessage_createsCommandEcho)
{
    auto parser = createIterator("echo message");

    auto cmd = parser.readNext();

    cmd->arguments();

    ASSERT_CMD_EQ(cmd, "echo", "message");
}

TEST(CommandParser, echoMessageWithNewline_createsCommandEchoWithoutNewline)
{
    auto parser = createIterator("echo message\n");

    auto cmd = parser.readNext();

    ASSERT_CMD_EQ(cmd, "echo", "message");
}

TEST(CommandParser, exit466_createsCommandExit)
{
    auto parser = createIterator("exit 466\n");

    auto cmd = parser.readNext();

    ASSERT_CMD_EQ(cmd, "exit", "466");
}

TEST(CommandParser, exit_createsCommandExit_withEmptyArgs)
{
    auto parser = createIterator("exit");

    auto cmd = parser.readNext();

    ASSERT_CMD_EQ(cmd, "exit", "");
}

TEST(CommandParser, canParseTwoCommands_eachOnItsLine)
{
    auto parser = createIterator("echo exiting\nexit\n");

    auto cmdEcho = parser.readNext();
    auto cmdExit = parser.readNext();
    auto cmdNull = parser.readNext();

    ASSERT_CMD_EQ(cmdEcho, "echo", "exiting");
    ASSERT_CMD_EQ(cmdExit, "exit", "");

    ASSERT_EQ(nullptr, cmdNull);
}

TEST(CommandParser, fs_create_createsCommand_FSCreate)
{
    auto parser = createIterator("fs@create myfile");

    auto cmd = parser.readNext();

    ASSERT_CMD_EQ(cmd, "fs@create", "myfile");
}

/**************************************************/

class MockShellParser: public Zen::ShellParser
{
public:
    MOCK_CONST_METHOD1(readCommandName, std::string(const std::string& line));
    MOCK_CONST_METHOD2(readCommandArgs, std::string(const std::string&, std::string));
    MOCK_CONST_METHOD1(getCommandFile, std::string(const std::string& fileName));
    MOCK_CONST_METHOD1(buildCommandInterface, Zen::Command*(const std::string&));
};

TEST(CommandParser, whenHaveCommandCreate_readNext_findsCommandInterface)
{
    //a command in the like of "create file" seeks "cif" file and looks up "create"
    auto parser = std::make_shared<MockShellParser>();
    auto iter = createIterator("fs@create myfile", parser);

    auto cmd = new Zen::CreateCommand;

    EXPECT_CALL(*parser, readCommandName("fs@create myfile"))
            .WillOnce(Return("fs@create"));

    EXPECT_CALL(*parser, getCommandFile("fs@create"))
            .WillOnce(Return("content"));

    EXPECT_CALL(*parser, buildCommandInterface("content"))
            .WillOnce(Return(cmd));

    EXPECT_CALL(*parser, readCommandArgs("fs@create", "fs@create myfile"))
            .WillOnce(Return("myfile"));

    std::unique_ptr<Zen::Command> actual = iter.readNext();

    ASSERT_EQ(cmd, actual.get());
}


int main(int argc, char* argv[])
{
        ::testing::InitGoogleMock(&argc, argv);

        return RUN_ALL_TESTS();
}
