#pragma once

#include <string>


namespace Zen {

class Command {
public:
    virtual ~Command() = default;

    virtual int execute(std::string&) = 0;
    virtual std::string name() const = 0;

    std::string arguments() const { return args; }
    void setArguments(const std::string& arguments) { args = arguments; }

private:
    std::string args;
};

}
