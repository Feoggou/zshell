#pragma once

#include "Command.h"

#include <string>

namespace Zen {
// NOTE: Temporary class (will be read from a script file)
class EchoCommand: public Command
{
public:
    int execute(std::string& stdOut) override
    {
        stdOut += arguments() + '\n';
        return 0;
    }

    std::string name() const override { return "echo"; }
};

}
