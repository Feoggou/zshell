#pragma once

#include "Command.h"

#include <string>

namespace Zen {
// NOTE: This is a temporary class (will be read from a script file)
class CreateCommand: public Command
{
public:
    int execute(std::string& stdOutContent) override;

    //TODO: The correct name is "create". However, we haven't implemented namespaces yet, so ATM it's okay.
    std::string name() const override { return "fs@create"; }
};

}
