#include "Create.h"

#include <fstream>


int Zen::CreateCommand::execute(std::string&)
{
    /*TODO: perhaps I should wrap this call, so I would be able to unit test this function.
     *      however, this is a temporary class, so I'm not sure about it. Perhaps when I get there.
     */
    std::ofstream stream;

    stream.open(arguments(), std::ios::out);

    return 0;
}
