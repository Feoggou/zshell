#pragma once

#include "TextFile.h"


namespace Zen {

class ConcreteTextFile: public TextFile
{
public:
    std::string readAll(const std::string& filePath) override;
};

}
