#pragma once

#include <string>


namespace Zen {
class TextFile
{
public:
    virtual ~TextFile() {}
    virtual std::string readAll(const std::string& filePath) = 0;
};

}
