#include "ConcreteTextFile.h"

#include <fstream>
#include <filesystem>

std::string Zen::ConcreteTextFile::readAll(const std::string& filePath)
{
    std::ifstream stream(filePath);

    if (!std::filesystem::exists(filePath)) {
        std::string errorMsg = "File '" + filePath + "' does not exist";
        throw std::runtime_error(errorMsg);
    }

    std::string content = { std::istreambuf_iterator<char>(stream), std::istreambuf_iterator<char>() };

    return content;
}
