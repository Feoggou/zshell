/*
*  Copyright 2015 Samuel Ghineț
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

#include "zLib/ConcreteTextFile.h"

#include "gmock/gmock.h"


TEST(TextFile, readAll)
{
    Zen::ConcreteTextFile textFile;
    std::string actual = textFile.readAll("nonempty.file");

    ASSERT_EQ("this file is not empty.\n"
              "don't believe? Read it!\n"
              "\n"
              "Yeah, I was right. See now?\n"
              "Seer.\n"
              "\n", actual);


}

TEST(TextFile, whenFileDoesNotExist_readAll_throwsException)
{
    Zen::ConcreteTextFile textFile;

    ASSERT_THROW(textFile.readAll("<file not existing >"), std::exception);
}


int main(int argc, char* argv[])
{
        ::testing::InitGoogleMock(&argc, argv);

        return RUN_ALL_TESTS();
}
